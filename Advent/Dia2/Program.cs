﻿using System;
using System.IO;

namespace Dia2
{
    internal class Program
    {
        static void Main()
        {
            Part1();
            Part2();
        }
        static void Part1()
        {
            using StreamReader sr = File.OpenText(@"\Users\CULLELL\Desktop\Advent\Dia2\Input.txt");
            string s;
            int id = 0;
            while ((s = sr.ReadLine()) != null)
            {
                string[] sa = s.Split(':');
                if (Corect(sa[1]))
                {
                    int sum;
                    int.TryParse(sa[0].Split(" ")[1], out sum);
                    id += sum;
                }
            }
            Console.WriteLine($"Part 1: {id}");
        }
        static bool Corect(string s)
        {
            bool red = true, blue = true, green = true;
            string[] saTurn = s.Split(';');
            for (int i = 0; i < saTurn.Length; i++)
            {
                string[] saDef = saTurn[i].Split(new char[] { ' ', ',' });
                for (int j = 2; j < saDef.Length; j += 3)
                {
                    switch (saDef[j])
                    {
                        case "red":
                            if (red) red = Convert.ToInt32(saDef[j - 1]) <= 12;
                            break;
                        case "blue":
                            if (blue) blue = Convert.ToInt32(saDef[j - 1]) <= 14;
                            break;
                        case "green":
                            if (green) green = Convert.ToInt32(saDef[j - 1]) <= 13;
                            break;

                    }
                }
            }
            
            return (red && blue && green);
        }
        static void Part2()
        {
            using StreamReader sr = File.OpenText(@"\Users\CULLELL\Desktop\Advent\Dia2\Input.txt");
            string s;
            int id = 0;
            while ((s = sr.ReadLine()) != null)
            {
                string[] sa = s.Split(':');
                id += MinBox(sa[1]);
            }
            Console.WriteLine($"Part 2: {id}");
        }
        static int MinBox(string s)
        {
            int red = 0, blue = 0, green = 0;
            string[] saTurn = s.Split(';');
            for (int i = 0; i < saTurn.Length; i++)
            {
                string[] saDef = saTurn[i].Split(new char[] { ' ', ',' });
                for (int j = 2; j < saDef.Length; j += 3)
                {
                    switch (saDef[j])
                    {
                        case "red":
                            if (red < Convert.ToInt32(saDef[j - 1])) red = Convert.ToInt32(saDef[j - 1]);
                            break;
                        case "blue":
                            if (blue < Convert.ToInt32(saDef[j - 1])) blue = Convert.ToInt32(saDef[j - 1]);
                            break;
                        case "green":
                            if (green < Convert.ToInt32(saDef[j - 1])) green = Convert.ToInt32(saDef[j - 1]);
                            break;

                    }
                }
            }

            return (red * blue * green);
        }
    }

}
