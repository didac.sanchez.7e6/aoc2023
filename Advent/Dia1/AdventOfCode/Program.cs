﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode
{
    internal class Program
    {
        static void Main()
        {
            Part2();
        }
        static void Part1()
        {
            using StreamReader sr = File.OpenText(@"\Users\CULLELL\Desktop\Advent\Dia1\Input.txt");
            string s;
            int suma = 0;

            while ((s = sr.ReadLine()) != null)
            {

                suma += FindNumber(s);
            }
            Console.WriteLine(suma);
        }
        static int FindNumber(string line)
        {
            int part = 0, i = 0, number;
            while (!int.TryParse(line.Substring(i, 1), out number))
            {
                i++;
            }
            part += number * 10;
            i = line.Length - 1;
            while (!int.TryParse(line.Substring(i, 1), out number))
            {
                i--;
            }
            part += number;
            return part;
        }
        static void Part2()
        {
            using StreamReader sr = File.OpenText(@"\Users\CULLELL\Desktop\Advent\Dia1\Input.txt");
            string s;
            float suma = 0;

            while ((s = sr.ReadLine()) != null)
            {

                suma += FindNumberWhitLetters(s);
            }
            Console.WriteLine(suma);
        }
        static int FindNumberWhitLetters(string line)
        {
            List<int> numeros = new List<int>();
            string[] numbers = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
            for (int i = 0; i < numbers.Length; i++)
            {
                if (line.Contains(numbers[i]))
                {
                    numeros.Add(line.IndexOf(numbers[i]));
                    numeros.Add(i % 9 + 1);
                    numeros.Add(line.LastIndexOf(numbers[i]));
                    numeros.Add(i % 9 + 1);
                }
            }


            return Index(numeros);
        }
        static int Index(List<int> numbers)
        {
            int first = 99, last = 0, bigger = 0, smol = 0;
            for (int i = 0; i < numbers.Count; i += 2)
            {
                if (numbers[i] < first)
                {
                    first = numbers[i];
                    bigger = numbers[i + 1] * 10;
                }
                if (numbers[i] >= last)
                {
                    last = numbers[i];
                    smol = numbers[i + 1];
                }
            }
            Console.WriteLine(bigger + smol);
            return bigger + smol;
        }
    }
}
